#!/bin/sh

echo "Getting urlzap"
wget -O -  https://github.com/brunoluiz/urlzap/releases/download/v1.0.0/urlzap_1.0.0_linux_amd64.tar.gz | tar zxvf -
ls -al

echo "Creating public-new folder"
mkdir public-new

echo "Executing urlzap"
./urlzap generate; RETCODE=$?
echo "Return Code is $RETCODE"

#if [ $RETCODE ] 
#then
  echo "Success!  Juggling folders."
  mv public public-rm
  mv public-new public
  
  echo "Setting read-only permission on public"
  chmod 444 public

  echo "Removing old public"
  rm -rf public-rm
#fi

